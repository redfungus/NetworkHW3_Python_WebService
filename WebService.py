import sys
import socket
from pathlib2 import Path
import subprocess


def send_file(sock, file_):
    sock.send("HTTP/1.1 200 OK\nContent-type: application/force-download\n\n")
    print 'reaches a'
    sock.send(file_.read())
    print 'reaches b'


mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('localhost', 10000)
print >> sys.stderr, 'starting up on %s port %s' % server_address
mySocket.bind(server_address)

mySocket.listen(5)

while True:
    # Wait for a connection
    print >>sys.stderr, 'waiting for a connection'
    connection, client_address = mySocket.accept()

    print >> sys.stderr, 'connection from', client_address
    # Receive the data in small chunks and retransmit it
    data = connection.recv(1024)
    print 'received \n"%s"' % data
    GET, rest = data.split('\r\n', 1)
    filename = data.split(' ')[1].split('/')[1]
    myFile = Path(filename)
    if myFile.is_file():
        print 'File is here'
        send_file(connection, open(filename))
    else:
        print 'File not here'
        connection.send("HTTP/1.1 404 FILE NOT FOUND\n")


